package com.mostblind.anagram;

import com.google.common.base.Function;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.Normalizer;
import java.util.*;

import static com.google.common.collect.Lists.transform;

public class AnagramService {

    public static Multimap<String, String> findAnagramsInFile(String path) throws IOException {
        return getMapOfAnagrams(getFileContentAsList(path));
    }

    protected static Multimap<String, String> getMapOfAnagrams(List<String> wordList) {
        Multimap<String, String> anagramMap = HashMultimap.create();
        for (String word : wordList) {
            anagramMap.get(prepareStringForAnagramComparison(word)).add(word);
        }
        return anagramMap;
    }

    protected static List<String> getFileContentAsList(String path) throws IOException {
        List<String> wordList = Files.readAllLines(Paths.get(path), Charset.forName("UTF-8"));
        return transform(wordList, new Function<String, String>() {
            @Override public String apply(String word) {
                return removeAccents(word.toLowerCase());
            }
        });
    }

    private static String prepareStringForAnagramComparison(String word) {
        char[] wordContent = word.toCharArray();
        Arrays.sort(wordContent);
        return new String(wordContent);
    }

    private static String removeAccents(String word) {
        return Normalizer.normalize(word, Normalizer.Form.NFD).replaceAll("\\p{IsM}", "");
    }

    public static void main(String args[]) {
        Multimap<String, String> anagramsMap = HashMultimap.create();

        if (args.length > 0) {
            try {
                anagramsMap = findAnagramsInFile(args[0]);
            } catch (IOException e) {
                System.out.println("Ugyldig fil: " + args[0]);
                System.exit(0);
            }

            if (anagramsMap.size() == 0) {
                System.out.println("Ingen anagrammer i tekstfil.");
            } else {
                System.out.println("Anagrammer funnet i tekstfil:\n");
                for (String word : anagramsMap.keySet()) {
                    if (anagramsMap.get(word).size() > 1) {
                        System.out.println(anagramsMap.get(word));
                    }
                }
            }
        } else {
            System.out.println("Sti til fil må angis som argument.");
            System.exit(0);
        }

    }

}
