package com.mostblind.anagram;

import com.google.common.collect.Multimap;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.junit.Assert.*;

public class AnagramServiceTest {

    private AnagramService anagramService;

    @Before
    public void setup() {
       anagramService = new AnagramService();
    }

    @Test
    public void findAnagramsInFileEndToEndTest() throws IOException {
        Multimap<String, String> anagramsMap = anagramService.findAnagramsInFile(getClass().getResource("/words.txt").getPath());

        assertEquals(3, anagramsMap.get("acehilm").size());
        assertEquals(2, anagramsMap.get("aceehrt").size());
        assertEquals(4, anagramsMap.get("aiprs").size());
        assertEquals(3, anagramsMap.get("aelpp").size());
        assertEquals(5, anagramsMap.get("aegnor").size());
    }

    @Test
    public void fileContentShouldBeReturnedAsList() throws IOException {
        List<String> wordList = anagramService.getFileContentAsList(getClass().getResource("/words.txt").getPath());
        assertNotNull(wordList);
        assertEquals(28, wordList.size());
    }

    @Test(expected = NullPointerException.class)
    public void callingFindAnagramWithInvalidFilePathShouldThrowIOExeption() throws IOException {
        anagramService.getFileContentAsList(getClass().getResource("src.txt").getPath());
    }

    @Test
    public void testFindAnagramsInWordList() {
        Multimap<String, String> anagramList = anagramService.getMapOfAnagrams(getWordList());

        assertNotNull(anagramList);
        assertTrue(anagramList.size() > 0);
        assertEquals(2, anagramList.get("acehilm").size());
        assertTrue(anagramList.get("acehilm").contains("leachim"));
        assertEquals(3, anagramList.get("amt").size());
        assertTrue(anagramList.get("amt").contains("atm"));
    }

    private List<String> getWordList() {
        List<String> wordList = new ArrayList<String>();
        wordList.add("michael");
        wordList.add("michael");
        wordList.add("leachim");
        wordList.add("leachim");
        wordList.add("finn");
        wordList.add("finne");
        wordList.add("finne");
        wordList.add("mat");
        wordList.add("mat");
        wordList.add("tam");
        wordList.add("atm");
        wordList.add("tam");

        return wordList;
    }
}
